#!/bin/bash

USERNAME="$1"

validateUserName() {
  if [ ! "$USERNAME" ]; then
    printf "No username provided!\n"
    exit 1
  fi
}

getRegion() {
  AWS_REGION="$(curl -s http://169.254.169.254/latest/dynamic/instance-identity/document | jq -r .region)"
}

getHashedPasswordFromParameterStore() {
  HASHED_PASSWORD="$(aws ssm get-parameter \
                       --name "/jumpbox/users/${USERNAME}" \
                       --with-decryption \
                       --region $AWS_REGION \
                       |jq -r .Parameter.Value)"
  if [ ! "$HASHED_PASSWORD" ]; then
    printf "Error getting hashed password for parameter store parameter: /jumpbox/users/${USERNAME}!\n"
    exit 1
  fi
}

deleteHashedPasswordFromParameterStore() {
  aws ssm delete-parameter \
    --name "/jumpbox/users/${USERNAME}" \
    --region $AWS_REGION
  if [ "$?" != "0" ]; then
    printf "Error deleting parameter store parameter: /jumpbox/users/${USERNAME}!\n"
    exit 1
  fi
}

addUser() {
  adduser -d /home/${USERNAME} -e `date -d "1 days" +"%Y-%m-%d"` ${USERNAME}
  mkdir -p /home/${USERNAME}
  chown ${USERNAME}: /home/${USERNAME}
  chage -M 1 ${USERNAME}
}

setUserPassword() {
  echo "${USERNAME}:${HASHED_PASSWORD}" | chpasswd -e
}

scheduleUserDeletion() {
cat <<EOF | at now + 1 days
pkill -u ${USERNAME}
sleep 5
userdel -f -r ${USERNAME}
EOF
}

validateUserName
getRegion
getHashedPasswordFromParameterStore
deleteHashedPasswordFromParameterStore
addUser
setUserPassword
scheduleUserDeletion
