execute "install epel" do
  command "amazon-linux-extras install -y epel"
end

yum_package "tigervnc-server" do
  action :install
end

yum_package "xterm" do
  action :install
end

yum_package "gnome-shell" do
  action :install
end

yum_package "gnome-session" do
  action :install
end

yum_package "nautilus" do
  action :install
end

yum_package "xrdp.x86_64" do
  action :install
end

cookbook_file '/etc/xrdp/xrdp.ini' do
  source 'etc/xrdp/xrdp.ini'
  owner 'root'
  group 'root'
  mode '0644'
  action :create
end

cookbook_file '/etc/xrdp/startwm.sh' do
  source 'etc/xrdp/startwm.sh'
  owner 'root'
  group 'root'
  mode '0755'
  action :create
end

execute "start xrdp" do
  command "systemctl enable --now xrdp"
end

cookbook_file '/root/create-temp-user.sh' do
  source 'root/create-temp-user.sh'
  owner 'root'
  group 'root'
  mode '0700'
  action :create
end

execute "install mate" do
  command "amazon-linux-extras install -y mate-desktop1.x"
end

execute "set mate default" do
  command "bash -c 'echo PREFERRED=/usr/bin/mate-session > /etc/sysconfig/desktop'"
end

service "xrdp" do
  action :restart
end
