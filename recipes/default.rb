execute "yum update" do
  command "yum update -y"
end

include_recipe 'jumpbox::xrdp'

yum_package "chromium" do
  action :install
end

yum_package "jq" do
  action :install
end
